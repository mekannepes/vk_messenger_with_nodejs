const express = require("express")
const jsonWebToken = require("jsonwebtoken")
const {jwtSecret} = require("../configs/app")
const {Pool} = require("pg")
const routAuth = express.Router()

const pool = new Pool({
  user: 'postgres',
  host: 'localhost',
  database: 'messenger',
  password: 'mekannepes',
  port: 5432,
})

routAuth.post("/register", async (req, res) => {

    const {name,surname,email, password} = req.body

    try {

        const user = await pool.query("select login from users where login = $1",[email])
        if(user.rows.length > 0){
            res.status(401).json({msg:"error"})
            return 
        }

        const text = "insert into users(user_id,login,password) values($1,$2,$3)"
        const values = []
    
        let r = Math.random().toString(36).substr(2, 3) + Math.random().toString(36).substr(2, 3) + Math.random().toString(36).substr(2, 4);    
        let userID = "id"+r
        values.push(userID)
        values.push(email)
        values.push(password)

        await pool.query(text,values)
        await pool.query("insert into info(user_id,name,surname) values($1,$2,$3)",[userID,name,surname])

        const token = jsonWebToken.sign(userID,jwtSecret)

        res.status(200).send({
            msg:"OK",
            "token":token
        })
        return
    } catch (error) {
        console.log(error)
        res.status(401).send()
        return 
    }

});

routAuth.post("/login", async (req, res) => {

    const {email, password} = req.body

    try {
        const user = await pool.query("select user_id,login,password from users where login = $1",[email])
        if(user.rows.length > 0){
            if(user.rows[0].login == email  &&  user.rows[0].password == password){
                const token = jsonWebToken.sign(user.rows[0].user_id,jwtSecret)

                return res.json({
                    userID:user.rows[0].user_id,
                    "token":token
                })
            }else{
                res.status(401).send()
                return
            }       
        
        }else{
            res.status(401).send()
            return 
        }
    } catch (error) {
        res.status(401).send()
        return 
    }
});

module.exports = routAuth 