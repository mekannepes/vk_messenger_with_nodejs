const express = require("express")
const jsonWebToken = require("jsonwebtoken")
const {Pool} = require("pg")
const router = express.Router()

const pool = new Pool({
  user: 'postgres',
  host: 'localhost',
  database: 'messenger',
  password: 'mekannepes',
  port: 5432,
})


router.get("/users", async (req, res) => {

    const autHeader = req.get("Authorization")

    const token = autHeader.replace("Bearer ","")
    let userID = jsonWebToken.decode(token)

    try {
        const user = await pool.query(`select u.user_id, i.name, i.surname from users u 
            join info i on i.user_id = u.user_id where u.user_id != $1
            and not exists(
            select * from friends where friend_one = $2 and friend_two = u.user_id or friend_one = u.user_id and friend_two = $3
        )`,[userID,userID,userID])
        user.rows.map(u=>{
            u.imgURL = "/images/camera_50.png"
            return (
                u
            )
        })
        res.json(user.rows)

    } catch (error) {
        res.status(401).send()
        return 
    }
    return

});

router.get("/friends", async (req, res) => {

    const autHeader = req.get("Authorization")

    const token = autHeader.replace("Bearer ","")

    let userID = jsonWebToken.decode(token)

    try {
        const user = await pool.query(`select u.user_id, i.name, i.surname from users u 
            join info i on i.user_id = u.user_id where u.user_id != $1
            and exists(
            select * from friends where friend_one = $2 and friend_two = u.user_id or friend_one = u.user_id and friend_two = $3
        )`,[userID,userID,userID])

        user.rows.map(u=>{
            u.imgURL = "/images/camera_50.png"
            return (
                u
            )
        })
        res.json(user.rows)
        return

    } catch (error) {
        console.log("Osibka")
        res.sendStatus(401)
        return 
    }
});

router.post("/friend", async (req, res) => {


    const autHeader = req.get("Authorization")

    const token = autHeader.replace("Bearer ","")

    let userID = jsonWebToken.decode(token)
    let friend_id = req.body.user_id

    try {
        await pool.query("insert into friends(friend_one,friend_two,status) values($1,$2,$3)",[userID,friend_id,0])

        res.sendStatus(200)
        return

    } catch (error) {
        console.log("Osibka")
        res.sendStatus(401)
        return 
    }
});

module.exports = router