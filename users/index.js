const express = require("express")
const AuthMiddlewear = require("./middlewear/auth")
const routeAuth = require('./routers/auth')
const api = require("./routers/api")
const app = express()

app.use(express.json())

app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods','GET, POST, PATCH, PUT, DELETE, OPTIONS');
    res.setHeader('Access-Control-Allow-Headers', 'origin, content-type, accept, Authorization, X-HTTP-Method-override');

    if('OPTIONS' == req.method){
        res.sendStatus(200)
    }else{
        next();
    }

})
//app.use(AuthMiddlewear)
app.use("/", routeAuth)
app.use("/api", AuthMiddlewear,api)

app.listen(3030, () => {
    console.log("Server has been started on port 3030")  
})