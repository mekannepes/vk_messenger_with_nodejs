
const http = require("http")
const socketIO = require("socket.io")
const jsonWebToken = require("jsonwebtoken")
const axios = require("axios")

const server = http.createServer()
const io = socketIO(server)

//clients
const users = new Map()

//sockets
io.use((socket, next) => {
    console.log(socket.handshake.query.token)
    next()
});

io.on("connection", (socket) => {
    console.log("user connected. User id: ", socket.id)

    const token = socket.handshake.query.token.replace("Bearer ","")
    
    let userID = jsonWebToken.decode(token)

    const connectionID = socket.id
    users.set(userID,connectionID)

    socket.on("addfriend", async (data) => {
        console.log("Geldi")
        try {
            await axios.post("http://localhost:3030/api/friend",
                {
                    user_id:data.user_id
                },
                {
                    mode: 'no-cors',
                    headers:{
                        Authorization: socket.handshake.query.token         
                    }
                })
                socket.emit("friendAdded",{added:true})
                let idConn = users.get(data.user_id)
                io.sockets.connected[idConn].emit("addrequest",{user_id:userID})
        } catch (error) {
            socket.emit("friendAdded",{added:false})
        }
    })

    socket.on("disconnect", (data) => {
        console.log("user disconnected")
        users.delete(userID)
    })
})

//starting server
server.listen(3001, () => {
    console.log("server has been started on port 3001");
})